$(document).ready(function () {
    //show menu
    $('.navbar-toggle').on('click', function () {
        $(this).toggleClass('active');
        $('.navbar-collapse').toggleClass('show');
    });
    $('.navbar-collapse .close,.navbar-nav a').on('click', function () {
        $('.navbar-collapse').removeClass('show');
        $('.navbar-toggle').removeClass('active');
    });

    $('.navbar-nav a').on('click',function (){
        $('.navbar-nav a').removeClass('active');
       $(this).addClass('active');
    });
});

