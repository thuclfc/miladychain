/*!
 * project_shg
 * 
 * 
 * @author Thuclfc
 * @version 2.0.0
 * Copyright 2023. MIT licensed.
 */$(document).ready(function () {
  //show menu
  $('.navbar-toggle').on('click', function () {
    $(this).toggleClass('active');
    $('.navbar-collapse').toggleClass('show');
  });
  $('.navbar-collapse .close,.navbar-nav a').on('click', function () {
    $('.navbar-collapse').removeClass('show');
    $('.navbar-toggle').removeClass('active');
  });
  $('.navbar-nav a').on('click', function () {
    $('.navbar-nav a').removeClass('active');
    $(this).addClass('active');
  });
});